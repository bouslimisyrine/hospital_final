<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ordonnance
 *
 * @ORM\Table(name="ordonnance")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrdonnanceRepository")
 */
class Ordonnance
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="dateOrd", type="string", length=255)
     */
    private $dateOrd;

    /**
     * @var string
     *
     * @ORM\Column(name="contenuOrd", type="string", length=255)
     */
    private $contenuOrd;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateOrd
     *
     * @param string $dateOrd
     *
     * @return Ordonnance
     */
    public function setDateOrd($dateOrd)
    {
        $this->dateOrd = $dateOrd;

        return $this;
    }

    /**
     * Get dateOrd
     *
     * @return string
     */
    public function getDateOrd()
    {
        return $this->dateOrd;
    }

    /**
     * Set contenuOrd
     *
     * @param string $contenuOrd
     *
     * @return Ordonnance
     */
    public function setContenuOrd($contenuOrd)
    {
        $this->contenuOrd = $contenuOrd;

        return $this;
    }

    /**
     * Get contenuOrd
     *
     * @return string
     */
    public function getContenuOrd()
    {
        return $this->contenuOrd;
    }
}

