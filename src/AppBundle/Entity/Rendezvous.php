<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rendezvous
 *
 * @ORM\Table(name="rendezvous")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RendezvousRepository")
 */
class Rendezvous
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="dateRdv", type="string", length=255)
     */
    private $dateRdv;

    /**
     * @var string
     *
     * @ORM\Column(name="lieuRdv", type="string", length=255)
     */
    private $lieuRdv;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateRdv
     *
     * @param string $dateRdv
     *
     * @return Rendezvous
     */
    public function setDateRdv($dateRdv)
    {
        $this->dateRdv = $dateRdv;

        return $this;
    }

    /**
     * Get dateRdv
     *
     * @return string
     */
    public function getDateRdv()
    {
        return $this->dateRdv;
    }

    /**
     * Set lieuRdv
     *
     * @param string $lieuRdv
     *
     * @return Rendezvous
     */
    public function setLieuRdv($lieuRdv)
    {
        $this->lieuRdv = $lieuRdv;

        return $this;
    }

    /**
     * Get lieuRdv
     *
     * @return string
     */
    public function getLieuRdv()
    {
        return $this->lieuRdv;
    }
}

