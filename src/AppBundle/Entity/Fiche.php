<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fiche
 *
 * @ORM\Table(name="fiche")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FicheRepository")
 */
class Fiche
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="dateCreation", type="string", length=255)
     */
    private $dateCreation;

    /**
     * @var string
     *
     * @ORM\Column(name="diagnostique", type="string", length=255)
     */
    private $diagnostique;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreation
     *
     * @param string $dateCreation
     *
     * @return Fiche
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set diagnostique
     *
     * @param string $diagnostique
     *
     * @return Fiche
     */
    public function setDiagnostique($diagnostique)
    {
        $this->diagnostique = $diagnostique;

        return $this;
    }

    /**
     * Get diagnostique
     *
     * @return string
     */
    public function getDiagnostique()
    {
        return $this->diagnostique;
    }
}

